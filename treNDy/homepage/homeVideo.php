<!DOCTYPE html>
<?php 
session_start(); 
$_SESSION['user_id'] = '';
?>

<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
  box-sizing: border-box;
}

body {
  margin: 0;
  //font-family: Arial;
  font-size: 17px;
}

#myVideo {
  position: fixed;
  right: 0;
  bottom: 0;
  min-width: 100%; 
  min-height: 100%;
}


#overlay {
  position: fixed; /* Sit on top of the page content */
  display: none; /* Hidden by default */
  width: 100%; /* Full width (cover the whole page) */
  height: 100%; /* Full height (cover the whole page) */
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0,0,0,0.5); /* Black background with opacity */
  z-index: -1; /* Specify a stack order in case you're using a different order for other elements */
  cursor: pointer; /* Add a pointer on hover */
}

.text{
  position: fixed;
  top: 23%;
  left:15%;
  right: 15%;
  font-size: 50px;
  color: white;
  //transform: translate(-50%,-50%);
  //-ms-transform: translate(-50%,-50%);
  text-align: center;
  margin-top: 1em;
  margin-bottom: 1em;
  margin-left: 0;
  margin-right: 0;
  
}

.overlay-desc {
  background: rgba(0,0,0,0);
  position: absolute;
  top: 0; right: 0; bottom: 0; left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
}

h1 {
  color: white;
  font-family: 'Nobile', sans-serif;
  font-size: 9vw;
  //text-align: center;
  position: fixed;
   top: 10px;
}

p {
  color: white;
  font-family: 'Open Sans', sans-serif;
  font-size: 1.1rem;
  line-height: 1.7rem;
 
}

h3 {
  color: white;
  font-family: 'Nobile', sans-serif;
  font-size: 3vw;
  text-align: center;
  margin-top: 1em;
  
}




</style>
<title>treNDy</title>

</head>

<video autoplay muted loop id="myVideo">
  <source src="../NotreDameTimelapse.mp4" type="video/mp4">
  Your browser does not support HTML5 video.
</video>

<div class="overlay">
    <div id="text">TreNDy</div> 

    <div class="overlay-desc"> 
        <h1> treNDy </h1>
        <br><br>
         
        <div class="text">
        <p> Living in a dorm is like living in a huge walk in closet!  Need an SYR costume or formal dress?  Walk down the hall and someone will have one to borrow. treNDy is here to  help! Create an account to browse other's clothes and input your own. Remember, fashion is like eating, you shouldn't stick to the same menu :) </p>
        
     <!--   </div>  text -->


<section class="container" z-index=3>
<div class="login">
    <h3> Sign In </h3>

<form method="POST" action="check_login.php">

<p><input type="text" name="uname" value="" placeholder="Username"></p>

<p><input type="password" name="pass" value="" placeholder="Password"></p>

<p class="submit"><input type="submit" name="commit" value="Log In"></p>
</form>
</div>

 
<?php
unset($SESSION["user_id"]);
if($_POST["logout"] != "Logout"){
  session_start();
  $_SESSION=array();
    session_destroy();
?>

<p style="color:red">Logged Out</p>

<?php
          }
?>


<form action="http://db.cse.nd.edu/cse30246/treNDy/createUser/users.php">
<button type="submit">Create Account</button>
</form>

</div> <!-- text -->

</div> <!-- overlay-desc -->




</div> <!-- overlay --> 





</html>









