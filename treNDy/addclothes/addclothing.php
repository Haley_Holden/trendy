<!DOCTYPE html>

<?php
session_start();

//$thisPage = "addClothing";

?>
<html>

<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


<link rel="stylesheet" href="../style.css">

<title>Add Clothes to Profile</title>

</head>

<?php include("../navbar.php"); ?>


<?php
$nickname = $_SESSION["name"];
$user = $_SESSION["user_id"];
if ($nickname == ""){
    echo "<h1> Error!</h1> <b>You must be logged in</b> </br>";
    echo "<button onclick=\"window.location.href = 'http://db.cse.nd.edu/cse30246/treNDy/homepage/homeVideo.php?';\">Sign in</button>";
    die();
}

/*
    $link=mysqli_connect('localhost', 'hholden', 'hholden') or die('Could not connect: '.mysql_error());

    mysqli_select_db($link, 'hholden') or die('Could not select database ');

    echo 'Connected Successfully ';

    $user = $_SESSION["user_id"];


$pic_name = $_FILES['clothing_pic']['name'];

$target_dir = '/var/www/html/cse30246/treNDy/addclothes/images/';
$target_file = $target_dir . basename($_FILES['clothing_pic']['name']);

$line = 'python ../machineLearning.py ' . $target_file;
$command = escapeshellcmd($line);
$output = shell_exec($command);

$sql = "INSERT INTO clothes (description, picture_url) VALUES (' ', /$pic_name')";
//echo ($sql);
//
$result = mysqli_query($link, $sql) or die('Error: ' .mysql_error());

mysqli_free_result($result);

        $target_dir = '/var/www/html/cse30246/treNDy/addclothes/images/';
        $target_file = $target_dir . basename($_FILES['clothing_pic']['name']);
        $uploadOk = 1;
        $errors = [];
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

        if (isset($_POST['submit'])) {
                        $check = getimagesize($_FILES['clothing_pic']['tmp_name']);
                        if ($check !== false) {
                                        echo "Image uploaded.";
                                     $uploadOk =1;
                        } else {
                                    echo "file upload error.";
                                    $uploadOk =0;
                        }
        }

        if (file_exists($target_file)) {
                    echo "Sorry, file already exists.";
                    $uploadOk = 0;
        }

        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                        && $imageFileType != "gif" ) {
                        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                        $uploadOk = 0;
        }

        if ($_FILES["clothing_pic"]["size"] > 50000000) {
                        echo "Sorry, your file is too large.";
                      $uploadOk = 0;
        }

        if ($uploadOk == 0) {
                        echo "Sorry, your file was not uploaded.";
        } else {
                      if (move_uploaded_file($_FILES["clothing_pic"]["tmp_name"], $target_file)) {
                                    echo "The file ". basename( $_FILES["clothing_pic"]["name"]). " has been uploaded.";
                        } else  {
                                    echo "Sorry, there was an error uploading your file.";
                        }
        }

        mysqli_close($link);

 */
?>
<body>
<h1 style="text-align:center; position:relative; top:60px;">Add Clothing Item <?php echo $nickname ?> !</h1><br> <br><br>
<style>
.center {
		text-align: center;
		//font-family: luminari;
}
</style>
    <div class=center >
        <form class="aligncenter" action='additem.php' method='post' enctype='multipart/form-data'> 
        
        <br> 
        <img src='' id="clothing_preview" width="200px" />
        <br>

						<br><br>Description of clothes: <input type='textbox' name='description'/><br>
						Size:  <select name='size' />
						<option value='XS'>XS</option>
			            <option value='S'>S</option>
						<option value='M'>M</option>
						<option value='L'>L</option>
						<option value='XL'>XL</option>
						<option value='XXL'>XXL</option>
						<option value='0'>0</option>
						<option value='2'>2</option>
						<option value='4'>4</option>
						<option value='5'>5</option>
						<option value='6'>6</option>
						<option value='7'>7</option>
						<option value='8'>8</option>
						<option value='9'>9</option>
						<option value='10'>10</option>
						<option value='11'>11</option>
						<option value='12'>12</option>
						<option value='14'>14</option>
						<option value='16'>16</option>
						<option value='18'>18</option>
						<option value='N/A'>N/A</option>
						</select>
						</br>
                        Color: <input type='color' name='color'/><br>
                   <!--  <br><br><br>
<div class="w3-panel" style="background-color:#fofyf4; color:#76685f" >
<h3>< ?php echo $output ?></h3>
<p>IBM Watson determined that the article of clothing that you are uploading is < ?php echo $output ?></p>
</div> 
<br><br> -->


                        Type: <select  name='type' id='type'/>
                        <option value="Select" selected>Select a Type...</option>
						<option value="Top">Top</option>
						<option value="Pants">Pants</option>
						<option value="Skirt">Skirt</option>
						<option value="Dress">Dress</option>
						<option value="Outerwear">Outerwear</option>
						<option value="Shoes">Shoes</option>
						<option value="Accessory">Accessory</option>
						</select>
						<br>
						Category: <select name='cat' />
						<option value="Casual">Casual</option>
						<option value="Going Out">Going Out</option>
						<option value="Formal">Formal</option>
						<option value="Theme">Theme</option>
						<option value="Game Day">Game Day</option>	
						<option value="Professional">Professional</option>
						</select>
						<br>
						Wash Instructions: <select name='wash' />
						<option value="Dry Clean">Dry Clean</option>
						<option value="Machine Wash">Machine Wash</option>
						<option value="Contact Owner">Contact Owner</option>
						<option value="None">None</option>
						</select>
                        <br>
                            Picture of Clothing Item: <input type='file' name='clothing_pic' id='clothing_pic' accept="image/*" onchange="preview_image(event);" />
                             
                            <!--Picture of Clothing Item: <input type='file' name='clothing_pic' id='clothing_pic' accept="image/*" value=<?php echo $_FILES['clothing_pic']['name']?> onchange="preview_image(event);" /> -->
                            <!-- Picture of Clothing Item: <input type='file' name='clothing_pic' id='clothing_pic'  value="< ?php echo basename($_FILES['clothing_pic']['name'])?>" />  --> 


<br>
<br>


					<script type="text/javascript">
							
                            function preview_image(event) {
                                    console.log("in preview");
									var reader = new FileReader();
                                    reader.onload = function() {
                                        console.log("in preview2");
											var output = document.getElementById('clothing_preview');
											output.src = reader.result;
									}
 									reader.readAsDataURL(event.target.files[0]);
                            }

                    </script>

                        <br><br>

            <button style='background-color:#d2f8d2' name='submit'> Add Item! </button>
        </form>
    </div>


</body>

</html>



